import React from "react";
const StopBtn = (props) => {
  return <input type="button" value="Stop" onClick={props.handleStop}></input>;
};

export default StopBtn;
