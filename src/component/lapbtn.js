import React from "react";
const LapBtn = (props) => {
  return (
    <input
      type="button"
      value="Laps"
      onClick={props.handleLap}
      style={{ width: "170px", marginTop: "15px" }}
    ></input>
  );
};

export default LapBtn;
