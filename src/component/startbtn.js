import React from "react";
const StartBtn = (props) => {
  return (
    <input type="button" value="Start" onClick={props.handleStart}></input>
  );
};

export default StartBtn;
