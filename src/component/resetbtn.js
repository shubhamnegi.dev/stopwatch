import React from "react";
const ResetBtn = (props) => {
  return (
    <input
      type="button"
      value="Reset"
      onClick={props.handleReset}
      style={{ marginLeft: "10px", marginRight: "10px" }}
    ></input>
  );
};

export default ResetBtn;
